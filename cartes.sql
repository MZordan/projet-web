-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 20 Novembre 2017 à 12:08
-- Version du serveur :  10.1.19-MariaDB
-- Version de PHP :  5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `love_letter`
--

-- --------------------------------------------------------

--
-- Structure de la table `cartes`
--

CREATE TABLE `cartes` (
  `id_carte` int(2) NOT NULL,
  `nom` varchar(20) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `cartes`
--

INSERT INTO `cartes` (`id_carte`, `nom`, `description`) VALUES
(1, 'Garde', 'Choisissez un joueur et essayer de deviner la carte qu\\''il a en main (excepté le Garde). Si vous tombez juste, le joueur est éliminé.'),
(2, 'Prêtre', 'Regardez la main d''un autre joueur (si vous êtes plus de 2 joueurs, ne montre pas la main aux autres joueurs)'),
(3, 'Baron', 'Comparez votre carte avec celle d''un autre joueur, le joueur avec la carte la plus faible est éliminé de la manche'),
(4, 'Servante', 'Jusqu''a votre prochain tour, vous êtes protégé des effets des cartes des autres joueurs'),
(5, 'Prince', 'Choisissez un joueur (y compris vous), celui-ci défausse la carte qu''il a en main pour en piocher une nouvelle'),
(6, 'Roi', 'Échangez votre main avec un autre joueur de votre choix'),
(7, 'Comtesse', 'Si vous avez cette carte en main en même temps que le Roi ou le Prince, alors vous devez défaussez la Comtesse'),
(8, 'Princesse', 'Si vous défaussez cette carte, vous êtes éliminé de la manche');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `cartes`
--
ALTER TABLE `cartes`
  ADD PRIMARY KEY (`id_carte`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `cartes`
--
ALTER TABLE `cartes`
  MODIFY `id_carte` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
