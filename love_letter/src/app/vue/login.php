
<html>

    <head>
	
        <meta charset="UTF-8">
        <title>Login</title>
		<link href="../../../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="../../../plugins/css_perso.css" rel="stylesheet">
    </head>


<body class="login" background="../../../img/login.jpg">
	<div id="fond" class="container container-table">
		<div class="row" style="height: 25vh;">
		</div>
		<div class="row" style="height: 50vh;">
			<div class="col-md-3">
			</div>
			<div class="col-md-6 text-center">
				<div class="jumbotron">
					<h1>Bienvenue sur LoveLetter!</h1><br>
					<div  id="login">
						<form action="../../index.php/login" method="POST">
						<input type="text" name="user" class="text-center" placeholder="Nom de compte">
						<input type="password" name="pass" class="text-center" placeholder="Mot de passe">
						<input type="submit" name="login" class="login btn btn-primary" value="Se connecter" style="margin-top: 5px;">
						</form>
						<div class="login-help">
						<a href='' onclick="document.getElementById('login').style.display = 'none'; document.getElementById('inscription').style.display = 'block';return false;">Créer un compte</a>
						</div>
				    </div>
					<div id="inscription" style="display:none;">
						<form action="../../index.php/inscri" method="POST">
							<input type="text" name="user" class="text-center" placeholder="Nom de compte"><br>
							<input type="password" name="pass" class="text-center" placeholder="Mot de passe" style="margin-top: 5px;">
							<input type="password" name="pass2" class="text-center" placeholder="Vérification">
							<input type="submit" name="inscription" class="login btn btn-primary" value="Créer le compte" style="margin-top: 5px;">
							<div class="login-help">
								<a href='' onclick="document.getElementById('login').style.display = 'block'; document.getElementById('inscription').style.display = 'none';return false;">Se connecter</a>
							</div>
						</form>
					</div>
				  
				</div>
			</div>
		</div>
	</div>
</body>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
	<script src="../../../plugins/bootstrap/js/bootstrap.min.js"></script>