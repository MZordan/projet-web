
var loveletter = (function () {
    return{
        fonctions: {}
    };
})();

loveletter.fonctions = (function () {

    return{
        init: function () {
            setInterval(loveletter.fonctions.maj, 3000);
        },
		draw:function(){
			 alert("yep"); 
			 
			 $.ajax({
			   url : '../../index.php/draw',
			   type : 'POST',
			   dataType : 'text',
			   success : function(text, status){
				   alert("reussi");
				   }
		   
			});
		},
		jouer:function(origine, position){
			 alert("yep"); 
			 $.ajax({
			   url : '../../index.php/jouerCarte/'+origine+'/'+position,
			   type : 'POST',
			   dataType : 'text',
			   success : function(text, status){
				   alert("reussi");
				   }
			});
		},
		maj:function(){
			$.ajax({
			   url : '../../index.php/maj',
			   type : 'POST',
			   dataType : 'text',
			   success : function(text, status){
				   //alert(text);
				   var cartes = text.split("|");
				   var i = 0;
				   var nbr_joueurs = cartes[0];
				   var position = Number(cartes[1]);
				   var nbr_libres = cartes[2];
				   
				   document.getElementById("cartes_deck").innerHTML = "<p class='texte'><br>Il reste "+nbr_libres+" carte(s) dans le deck</p>";
				   var p = $('<p>').attr('class','texte').attr('innerHTML',nbr_libres);
				   $('#cartes_deck').append(p);
				   
				   while(cartes[i] != 'Fin'){
					   if(cartes[i] == 'Defausse'){
							i++;
							document.getElementById("defausse").innerHTML = "";
							while(cartes[i] != 'FinDefausse'){
								var img = $('<img>').attr('src',cartes[i]).attr('title',"Carte défausée")
									.attr('class','img-carte-main');
								$('#defausse').append(img);
								i++;
							}
					   }
					   if(cartes[i] == 'Main'){
							i++;
							document.getElementById("main_bas").innerHTML = "";
							while(cartes[i] != 'FinMain'){
								var img = $('<img>').attr('src',cartes[i]).attr('title',"Carte visible")
									.attr('onmouseover','glow(this,true)')
                                    .attr('onmouseout','glow(this,false)')
									.attr('class','img-carte-main-perso');
								i++;
								var ciblesString = "";
								while(cartes[i] != 'FinCible'){
									var positionJoueur = Number(cartes[i]);
									if(nbr_joueurs == 4){
										if(position + 1 == positionJoueur || position - 3 == positionJoueur){ var emplacement = "jGauche"; }
										if(position - 1 == positionJoueur || position + 3 == positionJoueur){ var emplacement = "jDroite";}
										if(position + 2 == positionJoueur || position - 2 == positionJoueur){ var emplacement = "jHaut";}
									}
									if(nbr_joueurs == 3){
										if(position + 1 == positionJoueur || position - 2 == positionJoueur){ var emplacement = "jGauche";}
										if(position - 1 == positionJoueur || position + 2 == positionJoueur){ var emplacement = "jDroite";}
									}
									if(nbr_joueurs == 2){
									   var emplacement = "jHaut";
									}
									ciblesString += 'target(this, "'+emplacement+ '");';
									i++;
								}
									img.attr('onclick',ciblesString);
								$('#main_bas').append(img);
								i++;
							}
							i++;
							document.getElementById("defausse_bas").innerHTML = "";
							while(cartes[i] != 'FinJoueur'){
								var img = $('<img>').attr('src',cartes[i]).attr('title',"Carte jouee")
									.attr('class','img-carte-defausse');
								$('#defausse_bas').append(img);
								i++;
							}
					   }
					   if(cartes[i] == 'AutreMain'){
						   i++;
						   var positionJoueur = Number(cartes[i]);
						   i++;
						   if(nbr_joueurs == 4){
							    if(position + 1 == positionJoueur || position - 3 == positionJoueur){ var emplacement = "gauche"; }
								if(position - 1 == positionJoueur || position + 3 == positionJoueur){ var emplacement = "droite";}
								if(position + 2 == positionJoueur || position - 2 == positionJoueur){ var emplacement = "haut";}
						   }
						   if(nbr_joueurs == 3){
								if(position + 1 == positionJoueur || position - 2 == positionJoueur){ var emplacement = "gauche";}
								if(position - 1 == positionJoueur || position + 2 == positionJoueur){ var emplacement = "droite";}
						   }
						   if(nbr_joueurs == 2){
							   var emplacement = "haut";
						   }
						   var main = 'main_'+emplacement;
						   var defausse = 'defausse_'+emplacement;
						   document.getElementById(main).innerHTML = "";
						   while(cartes[i] != 'AutreFinMain'){
								var img = $('<img>').attr('src','../../../img/dos_carte.jpg').attr('title',"Carte non-visible")
									.attr('class','img-carte-main');
								$("#"+main).append(img);
								i++;
							}
							i++;
							document.getElementById(defausse).innerHTML = "";
							while(cartes[i] != 'AutreFinJoueur'){
								var img = $('<img>').attr('src',cartes[i]).attr('title',"Carte jouee")
									.attr('class','img-carte-defausse');
								$("#"+defausse).append(img);
								i++;
							}
					   }
						i++;
				   }
			   }
			});
		}
	};
})();

$(document).ready(function() {
	loveletter.fonctions.init();
});
</script>
