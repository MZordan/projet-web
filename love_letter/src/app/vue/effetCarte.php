<?php

$desc_guard = "Choisissez un joueur et essayez de deviner la carte qu'il a en main (excepté le Garde). Si vous tombez juste, le joueur est éliminé.";

$desc_priest = "Regardez la main d'un autre joueur (si vous êtes plus de 2 joueurs, ne montre pas la main aux autres joueurs).";

$desc_barron = "Comparez votre carte avec celle d'un autre joueur, le joueur avec la carte la plus faible est éliminé de la manche.";

$desc_handmaid = "Jusqu'a votre prochain tour, vous êtes protégé des effets des cartes des autres joueurs.";

$desc_prince = "Choisissez un joueur (y compris vous), celui-ci défausse la carte qu'il a en main pour en piocher une nouvelle.";

$desc_king = 'Échangez votre main avec un autre joueur de votre choix.';

$desc_countess = 'Si vous avez cette carte en main en même temps que le Roi ou le Prince, alors vous devez défaussez la Comtesse.';

$desc_princess = 'Si vous défaussez cette carte, vous êtes éliminé de la manche.';


$tableau_effets = '<div class="table-responsive">
								<table class="table">
									<tr>
										<th class="text-center"> Force </th>
										<th class="text-center"> Nom </th>
										<th class="text-center"> Effet </th>
									</tr>
									<tr>
										<td class="text-center">1</td>
										<td class="text-center"> Garde</td>
										<td class="text-center">'.$desc_guard.'</td>
									</tr>
									<tr>
										<td class="text-center">2</td>
										<td class="text-center"> Pretre</td>
										<td class="text-center">' . $desc_priest . '</td>
									</tr>
									<tr>
										<td class="text-center">3</td>
										<td class="text-center"> Baron</td>
										<td class="text-center"> '.$desc_barron.'</td>
									</tr>
									<tr>
										<td class="text-center">4</td>
										<td class="text-center"> Servante</td>
										<td class="text-center"> '.$desc_handmaid.'</td>
									</tr>
									<tr>
										<td class="text-center">5</td>
										<td class="text-center"> Prince</td>
										<td class="text-center"> '.$desc_prince.'</td>
									</tr>
									<tr>
										<td class="text-center">6</td>
										<td class="text-center"> Roi</td>
										<td class="text-center"> '.$desc_king.'</td>
									</tr>
									<tr>
										<td class="text-center">7</td>
										<td class="text-center"> Comtesse</td>
										<td class="text-center"> '.$desc_countess.'</td>
									</tr>
									<tr>
										<td class="text-center">8</td>
										<td class="text-center"> Princesse</td>
										<td class="text-center"> '.$desc_princess.'</td>
									</tr>
								</table>
							</div>';
?>