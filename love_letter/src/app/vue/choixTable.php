<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<html>

    <head>
	
        <meta charset="UTF-8">
        <title>Login</title>
		<link href="../../../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="../../../plugins/css_perso.css" rel="stylesheet">
    </head>


<body class="login" background="../../../img/login.jpg">
	<div id="fond" class="container container-table">
		<div class="row" style="height: 25vh;">
		</div>
		<div class="row" style="height: 50vh;">
			<div class="col-md-2">
			</div>
			<div class="col-md-8 text-center">
				<div class="jumbotron">
					<h1>Bienvenue sur LoveLetter!<?php echo($_SESSION['id_joueur']);?></h1><br>
					<div  id="creer">
						<form action="../../index.php/login" method="POST">
						<div class="row">
							<div class="col-md-5">
							</div>
							<div class="col-md-1">
								<input type="submit" name="login" class="login btn btn-primary" value="Créer la salle" style="margin-top: 5px;">
							</div>
						</div>
						
						</form>
				    </div>
					<div>
						<table class="table">
						<thead>
						  <tr>
							<th>Joueurs présents</th>
							<th>Id de la partie</th>
							<th> </th>
						  </tr>
						</thead>
						<tbody id='tableau'>
						</tbody>
					  </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
	<script src="../../../plugins/bootstrap/js/bootstrap.min.js"></script>
	
	<script>
	var loveletter = (function () {
    return{
        fonctions: {}
    };
})();

loveletter.fonctions = (function () {

    return{
		tableau:function(){			 
			 $.ajax({
			   url : '../../index.php/tableau',
			   type : 'POST',
			   dataType : 'text',
			   success : function(text, status){
				   var div = document.getElementById('tableau');
					div.innerHTML += text;		
				   }
		   
			});
		}
		};
})();
$(document).ready(function() {
	loveletter.fonctions.tableau();
});

</script>