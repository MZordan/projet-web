<?php
namespace app\vue;
include 'effetCarte.php';
?> 

<html>

    <head>
	
        <meta charset="UTF-8">
        <title>Table de jeu</title>
		<link href="../../../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
		<link href="../../../plugins/css_perso.css" rel="stylesheet">
    </head>
	
    <body id="body" background="../../../img/table.jpg">
		<div id="fond" class="container-fluid">
		
			<div class="row" style="height: 25vh;">
				<div class="col-md-4">	
				
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Carte inconnue ?</button>

					<!-- Modal -->
					<div id="myModal" class="modal modal-large fade" role="dialog">
					  <div class="modal-dialog">

						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header">
							<h4 class="modal-title">Description des effets des cartes</h4>
							<button type="button" class="close" data-dismiss="modal">&times;</button>
						  </div>
						  <div class="modal-body">
							<?php echo($tableau_effets); ?>
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
						  </div>
						</div>
					  </div>
					</div>

				</div>
				
				<div id="jHaut" class="col-md-4">
					<div class="row">
						<div class="mx-auto">
							<p class="texte">Joueur Haut</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div id="main_haut" class="text-center">
								<img class="img-carte-main"  src="../../../img/dos_carte.jpg">
								<img class="img-carte-main"  src="../../../img/dos_carte.jpg">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div id="defausse_haut" class="text-center">
								<img class="img-carte-defausse" src="../../../img/1-guard.jpg">
								<img class="img-carte-defausse" src="../../../img/7-countess.jpg">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4" style="border-style: dotted; border-color: white;">
					<div class="row">
						<div class="col-md-4">
							<div id="cartes_deck" class="mx-auto">
								<p class="texte"><br>Il reste X carte(s) dans le deck</p>
							</div>
						</div>
						<div class="col-md-8">
							<div class="row">
								<div class="mx-auto">
									<p class="texte">Cartes défaussées</p>
								</div>
							</div>
							<div class="col-md-12">
								<div id="defausse" class="text-center">
									<img class="img-carte-main"  src="../../../img/dos_carte.jpg">
									<img class="img-carte-main"  src="../../../img/dos_carte.jpg">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row" style="height: 25vh;">
				<div id="jGauche" class="col-md-4">
					<div class="row">
						<div class="mx-auto">
							<p class="texte">Joueur Gauche</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div id="main_gauche" class="text-center">
								<img class="img-carte-main"  src="../../../img/dos_carte.jpg">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div id="defausse_gauche" class="text-center">
								<img class="img-carte-defausse" src="../../../img/1-guard.jpg">
								<img class="img-carte-defausse" src="../../../img/7-countess.jpg">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
				</div>
				<div id="jDroite" class="col-md-4">
					<div class="row">
						<div class="mx-auto">
							<p class="texte">Joueur Droite</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div id="main_droite" class="text-center">
								<img class="img-carte-main"  src="../../../img/dos_carte.jpg">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div id="defausse_droite" class="text-center">
								<img class="img-carte-defausse" src="../../../img/1-guard.jpg">
								<img class="img-carte-defausse" src="../../../img/7-countess.jpg">
								<img class="img-carte-defausse" src="../../../img/7-countess.jpg">
								<img class="img-carte-defausse" src="../../../img/7-countess.jpg">
								<img class="img-carte-defausse" src="../../../img/7-countess.jpg">
								<img class="img-carte-defausse" src="../../../img/7-countess.jpg">
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row" style="height: 33vh;">
				<div class="col-md-4">
				</div>
				<div id="jBas" class="col-md-4">
					<div class="row">
						<div class="mx-auto">
							<p class="texte">Joueur Bas</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div id="defausse_bas" class="text-center">
								<img class="img-carte-defausse" src="../../../img/1-guard.jpg">
								<img class="img-carte-defausse" src="../../../img/7-countess.jpg">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div id="main_bas" class="text-center">
								<img id="2" class="img-carte-main-perso" src="../../../img/2-priest.jpg" onmouseover="glow(this, true)" onmouseout="glow(this, false)" onclick="target(this)">
								<img id="1" class="img-carte-main-perso" src="../../../img/1-guard.jpg" onmouseover="glow(this, true)" onmouseout="glow(this, false)" onclick="target(this)">
							</div>
						</div>
					</div>
					
				</div>
			</div>
			
			
		</div>
    </body>
	
	
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.13.0/umd/popper.min.js"></script>
	<script src="../../../plugins/bootstrap/js/bootstrap.min.js"></script>
	
	<script>
		function glow(img, etat) {
			if(etat){
				img.style.boxShadow = "0px 0px 30px 2px #cf5";
			}else if(! img.classList.contains('selected') ){
				img.style.boxShadow = "0px 0px 0px 0px #cf5";
			}
		}
		
		function target(img, positionJoueur){
			$("body").addClass('darken');
			$("img").removeClass('selected');
			$(img).addClass('selected');
			img.style.boxShadow = "0px 0px 30px 2px #cf5";
			
			$("#"+positionJoueur).addClass('joueur-valide');
			
			handler = function(){ 
					alert(this.id + img.src.replace(/\D/g,''));
					loveletter.fonctions.jouer(img.src.replace(/\D/g,''), this.id);
					$("img").removeClass('selected');
					img.style.boxShadow = "0px 0px 0px 0px #cf5";
					$("body").removeClass('darken');
					$('#jDroite').removeClass('joueur-valide');
					$('#jBas').removeClass('joueur-valide');
					$('#jHaut').removeClass('joueur-valide');
					$('#jGauche').removeClass('joueur-valide');
					tmp = document.getElementById("jGauche");
					tmp.parentNode.replaceChild(tmp.cloneNode(true),tmp);
					tmp = document.getElementById("jHaut");
					tmp.parentNode.replaceChild(tmp.cloneNode(true),tmp);
					tmp = document.getElementById("jDroite");
					tmp.parentNode.replaceChild(tmp.cloneNode(true),tmp);
					tmp = document.getElementById("jHaut");
					tmp.parentNode.replaceChild(tmp.cloneNode(true),tmp);
				};
					
			tmp = document.getElementById(positionJoueur);
			tmp.parentNode.replaceChild(tmp.cloneNode(true),tmp);
			
			document.getElementById(positionJoueur).addEventListener("click", 
				handler,
				false);
			
			document.addEventListener("dblclick", 
				function(){ 
					$("img").removeClass('selected');
					img.style.boxShadow = "0px 0px 0px 0px #cf5";
					$("body").removeClass('darken');
					$('#jDroite').removeClass('joueur-valide');
					$('#jBas').removeClass('joueur-valide');
					$('#jHaut').removeClass('joueur-valide');
					$('#jGauche').removeClass('joueur-valide');
					tmp = document.getElementById("jGauche");
					tmp.parentNode.replaceChild(tmp.cloneNode(true),tmp);
					tmp = document.getElementById("jHaut");
					tmp.parentNode.replaceChild(tmp.cloneNode(true),tmp);
					tmp = document.getElementById("jDroite");
					tmp.parentNode.replaceChild(tmp.cloneNode(true),tmp);
					tmp = document.getElementById("jHaut");
					tmp.parentNode.replaceChild(tmp.cloneNode(true),tmp);
				});
		}
	
	<?php
		include 'fonctions.php';
	?>
	
	
</html>
