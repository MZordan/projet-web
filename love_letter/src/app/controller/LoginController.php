<?php
namespace app\controller;
use app\model\Player as player;

class LoginController {


    public function login($login, $pswd) {
        $id = player::where("login", $login)->where("pswd", $pswd)->first()->toArray();
        return count($id) > 0;
    }

}