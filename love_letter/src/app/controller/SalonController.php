<?php
namespace app\controller;
use app\model\Game as game;
use app\model\PlayerInGame as player_in_game;


class SalonController
{

    public function createGame($id_joueur, $nbJoueur) {
        $game = new game;
        $game->NbPlayer = $nbJoueur;
        $game->save();

        $id = $game["attributes"]["id"];
        $this->joinGame($id_joueur, $id);
    }

    public function joinGame($id_joueur, $id_game) {
        $place = player_in_game::where("IdGame", $id_game)->get()->toArray();


        $player_in_game = new player_in_game;
        $player_in_game->idPlayer  = $id_joueur;
        $player_in_game->idGame    = $id_game;
        $player_in_game->score     = 0;
        $player_in_game->eleminate = 0;
        $player_in_game->protected = 0;
        $player_in_game->place     = $place->count()+1;
        $player_in_game->save();

        $players = player_in_game::where("idGame", $id_game)->get()->toArray();
        $nbp = count($players);
        $game = game::find($id_game);
        if($nbp >= $game["attributes"]["nbPlayer"]) {
            $game->full = 1;
            $game->save();
        }
    }

    public function get_Game() {
        $ids = Array();
        $games = game::where("Full", 0)->get()->toArray();
        foreach ($games as $game) {
            $ids[] = $game["IdGame"];
        }
        return $ids;
    }

}