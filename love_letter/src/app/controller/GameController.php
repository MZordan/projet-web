<?php
namespace app\controller;
use app\model\Card as card;
use app\model\PlayerInGame as player_in_game;
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

	/*
	Deck composition :
		(5) Guard 
		(2) Priest
		(2) Barron
		(2) Handmaid
		(2) Prince
		(1) King
		(1) Countess
		(1)	Princess
	*/
class GameController {
	
    const STATE_LIB    = 0,
		  STATE_HAND   = 1,
		  STATE_PLAYED = 2,
		  STATE_HIDDEN = 4,
		  STATE_REVEAL = 8;

	const CARD_FUNCTION = array(
			"effect_guard",
			"effect_priest",
			"effect_barron",
			"effect_handmaid",
			"effect_prince",
			"effect_king",
			"effect_countess",
			"effect_princess"
    );

    public function deck_set($id_game) {
	    $cards = card::where("IdGame", $id_game)->get()->toArray();
		return count($cards) == 0;
    }

    public function distribute($array_players, $id_game) {
        foreach($array_players as $id_player) {
            $this->draw($id_player, $id_game);
        }
    }

    public function draw($id_player, $id_game) {
        $cards = card::where("idGame", $id_game)->where("state", self::STATE_LIB)->get();
        if(count($cards->toArray()) == 0) {
            $this->get_round_end($id_game);
        } else {
            $rand = rand(0, count($cards));
            $id   = $cards[$rand]["attributes"]['idC'];
            $card = card::find($id);
            $card->state = self::STATE_HAND;
            $card->idPlayer = $id_player;
            $card->save();
        }
    }

    public function eleminate_player($id_player, $id_game) { //bug a corriger
		$_SESSION['nbr_enJeu'] = $_SESSION['nbr_enJeu'] -1;
        $player_in_game = player_in_game::where("IdPlayer", $id_player)
            ->where("IdGame", $id_game)
            ->first();
        $p_in_g_id = $player_in_game["attributes"]["Id"];
        $player_in_game = player_in_game::find($p_in_g_id);
        $player_in_game->Eleminate = 1;
        $player_in_game->save();

        $cards = card::where("IdGame", $id_game)->where("IdPlayer", $id_player)->get();

        foreach($cards as $card) {
            $id   = $card["attributes"]['idC'];
            $card = card::find($id);
            $card->state = self::STATE_REVEAL;
            $card->save();
        }
    }

    public function get_player_from_place($positionJoueur, $id_game) {
		$position = $_SESSION['position'];
		if($positionJoueur == "jDroite"){
			if($position - 1 > 0){
				$emplacement = $position - 1;
			}else{
				$emplacement = $_SESSION['nbr_joueur'];
			}
		}
		if($positionJoueur == "jGauche"){
			if($position + 1 <= $_SESSION['nbr_joueur']){
				$emplacement = $position + 1;
			}else{
				$emplacement = 1;
			}
		}	
		if($positionJoueur == "jHaut"){
			if($position - 2 > 0){
				$emplacement = $position - 2;
			}else{
				$emplacement = $position + 2;
			}
		}	
        $player_in_game = player_in_game::where("Position", $emplacement)
            ->where("IdGame", $id_game)
            ->first();
        return $player_in_game["attributes"]["IdPlayer"];
    }
	
	public function get_place_from_player($player, $id_game){
		$player_in_game = player_in_game::where("IdPlayer", $player)
            ->where("IdGame", $id_game)
            ->first();
        return $player_in_game["attributes"]["Position"];
	}

    public function get_player_from_card($id_card) {
        $card_played = card::find($id_card);
        $id_player_1 = $card_played[0]["attribute"]["idPlayer"];
        return $id_player_1;
    }

    public function hide_card($id_game) {
        $cards = card::where("idGame", $id_game)->where( "state", self::STATE_LIB)->get();
        $rand = rand(0, count($cards));
        $id   = $cards[$rand]["attributes"]['idC'];
        $card = card::find($id);
        $card->state = self::STATE_HIDDEN;
        $card->save();
    }

    public function insert($id_game, $type) {
        $card = new card;
        $card->idGame = $id_game;
        $card->state = self::STATE_LIB;
        $card->type = $type;
        $card->save();
    }

    public function play_card($args) { // args : "cible": id de la cible // "type": type ciblé // "id_game": id de la game //card: type de la carte jouee
		$joueur = player_in_game::where("IdGame", $_SESSION['id_game'])->where("IdPlayer", $_SESSION['id_joueur'])->first();
		if($joueur['TourDeJeu'] == 0){		
			$card = card::where('IdPlayer',$_SESSION['id_joueur'])->where('Type', $args['card'])->where('State',self::STATE_HAND)->first();
			$card->state = self::STATE_PLAYED;
			$card->save();
			$args['id_card'] = $card['idC'];
			$args['id_game'] = $_SESSION['id_game'];
			$args["cible"] = $this->get_player_from_place($args["position"], $_SESSION['id_game']);
			call_user_func_array(array($this, self::CARD_FUNCTION[$type-1]), $args);
			$this->pass_turn();
		}
    }

    public function set_Deck($id_game) {
        //Add 5 guard to the game
        $this->insert($id_game, 1);
        $this->insert($id_game, 1);
        $this->insert($id_game, 1);
        $this->insert($id_game, 1);
        $this->insert($id_game, 1);
        //Add 2 priest to the game
        $this->insert($id_game, 2);
        $this->insert($id_game, 2);
        //Add 2 barron to the game
        $this->insert($id_game, 3);
        $this->insert($id_game, 3);
        //Add 2 handmaid to the game
        $this->insert($id_game, 4);
        $this->insert($id_game, 4);
        //Add 2 prince to the game
        $this->insert($id_game, 5);
        $this->insert($id_game, 5);
        //Add 1 king to the game
        $this->insert($id_game, 6);
        //Add 1 countess to the game
        $this->insert($id_game, 7);
        //Add 1 princess to the game
        $this->insert($id_game, 8);
    }

    public function setup($array_players, $id_game) {
        if($this->deck_set($id_game)) {
            $this->set_Deck($id_game);
        }

        $this->hide_card($id_game);

        if(count($array_players) == 2) {
            $this->setup_2_players($id_game);
        }

        //Draw
        $this->distribute($array_players, $id_game);
    }

    public function setup_2_players($id_game) {
        $i = 0;
        for(;$i < 3; $i++) {
            $cards = card::where("idGame" , $id_game)->where( "state" , self::STATE_LIB)->get();
            $rand = rand(0, count($cards));
            $id   = $cards[$rand]["attributes"]['idC'];
            $card = card::find($id);
            $card->state = self::STATE_REVEAL;
            $card->save();
        }
    }
	
	public function pass_turn(){
		$joueurs = player_in_game::where("IdGame", $id_game)->get();
		foreach($joueurs as $joueur){
			if($joueur['Eliminate'] == 0 ){
				$tmp = $joueur['TourDeJeu'] - 1;
				if($tmp ==-1){
					$joueur->TourDeJeu = $_SESSION['nbr_enJeu']-1;
				}else{
					$joueur->TourDeJeu = $tmp;
				}
				$joueur->save();
			}
		}
	}



//#############################################""CARD EFFECT ###################################"

    public function effect_guard ($args) { // args : "cible": id de la cible // "type": type ciblé // "id_game": id de la game
        $card = card::where("idGame", $args["id_game"])->where("idPlayer", $args["cible"])->where("Type", $args["type"])->first()->toArray();
        if(count($card) == 1) {
            $this->eleminate_player($args["cible"], $args["id_game"]);
        }
    }
		
    public function effect_priest($args) {}

    public function effect_barron($args) {// args : "cible": id de la cible // "id_card":id de la carte barron joué //"id_game": id de la game
        $id_player_1 = $this->get_player_from_card($args["id_card"]);
        $card1 = card::where("idPlayer", $id_player_1)->where("state", self::STATE_HAND)->get();
        $card2 = card::where("idPlayer", $args["cible"])->where("state", self::STATE_HAND)->get();
        if($card1[0]["attribute"]["type"] > $card2[0]["attribute"]["type"]) {
            $this->eleminate_player($args["cible"], $args["id_game"]);
        } elseif($card1[0]["attribute"]["type"] > $card2[0]["attribute"]["type"]) {
            $this->eleminate_player($id_player_1, $args["id_game"]);
        }
    }
		
    public function effect_handmaid($args) { // args : "id_card":id de la carte handmaid joué  // "id_game": id de la game
        //set protected to true
        $id_player = $this->get_player_from_card($args["id_card"]);
        $player_in_game = player_in_game::where("IdPlayer", $id_player)
            ->where("IdGame", $args["id_game"])
            ->first();
        $p_in_g_id = $player_in_game["attributes"]["Id"];
        $player_in_game = player_in_game::find($p_in_g_id);
        $player_in_game->protected = 1;
        $player_in_game->save();
    }

    public function effect_prince($args) { // args : "cible": id de la cible // "id_game": id de la game
        $card = card::where("idPlayer", $args["cible"])->where("state", self::STATE_HAND)->first();
        if($card[0]["attributes"]["type"] == 8) {
            $this->effect_princess(array("id_game" => $args["id_game"], "id_card" => $card));
        } else {
            $card->state = self::STATE_PLAYED;
            $card->save();
            $this->draw($args["cible"], $args["id_game"]);
        }
    }

    public function effect_king($args) { // args : "cible": id de la cible // "id_card" : id de la carte king joué // "id_game": id de la game
        $card_p2 = card::where("idPlayer", $args["cible"])->where("state", self::STATE_HAND)->first();
        $id_player_1 = $this->get_player_from_card($args["id_card"]);
        $card_p1_2 = card::where("idPlayer", $id_player_1)->where("state", self::STATE_HAND)->first();

        $card_p1_2->idPlayer = $args["cible"];
        $card_p1_2->save();
        $card_p2->idPlayer = $id_player_1;
        $card_p2->save();
    }
		
    public function effect_countess($args) {}

    public function effect_princess($args) { // args : "id_card": id de la carte princess joué // "id_game": id de la game
        $id_player = $this->get_player_from_card($args["id_card"]);
        $this->eleminate_player($id_player, $args["id_game"]);
    }

//#####################################END OF ROUND###################################""
    public function get_round_end($id_game) {
        $players = player_in_game::where("IdGame", $id_game)->where("Eleminate", 0)->get()->toArray();
        if(count($players) == 1) {
            $this->round_end($players[0]["Id"], $id_game);
        } else {
            $cards = card::where("idGame", $id_game)->where("state", self::STATE_LIB)->get()->toArray();
            if(count($cards) == 0) {
                $gagnants = $this->get_gagnant_score($id_game);
                if(count($gagnants) > 1) {
                    $gagnant = get_gagnant_by_hand($id_game, $gagnants);
                }
                $this->round_end($gagnant, $id_game);
            }
        }
    }

    public function get_gagnant_by_hand($id_game, $ids) {
        $max = 0;
        foreach ($ids as $id) {
            $card = card::where("IdGame", $id_game)->where("IdPlayer", $id)->where("state", self::STATE_HAND)->first()->toArray();
            if($card["type"] > $max) {
                $gagnant = $id;
                $max = $card["type"];
            }
        }
        return $gagnant;
    }

    public function get_gagnant_score($id_game) {
        $max = 0;
        $ids  = array();
        $players = player_in_game::where("IdGame", $id_game)->get();
        foreach ($players as $player) {
            $val = $this->get_cumulate_score($id_game, $player["attributes"]["IdPlayer"]);
            if($val = $max) {
                $ids[] =  $player["attributes"]["IdPlayer"];
                }
            if($val > $max) {
                unset($ids);
                $ids = array($player["attributes"]["IdPlayer"]);
                $max = $val;
            }
        }
        return $ids;
    }

    public function get_cumulate_score($id_game, $id_player) {
        $cumul = 0;
        $cards =  card::where("IdGame", $id_game)->where("IdPlayer", $id_player)->where("state", self::STATE_PLAYED)->get()->toArray();
        foreach ($cards as $card) {
            $cumul += $card["type"];
        }
        return $cumul;
    }

    public function round_end($id_winer, $id_game) {
        $gagnant = player_in_game::find($id_winer);
        $gagnant->score = $gagnant["attributes"]["Score"]+1;
        $gagnant->save();
        $this->reset_round($id_game);
    }


    public function reset_round($id_game) {
        $cards = card::where("idGame", $id_game)->get();
        foreach($cards as $card) {
            $id   = $card["attributes"]['idC'];
            $card = card::find($id);
            $card->state = self::STATE_LIB;
            $card->idPlayer = null;
            $card->save();
        }

        //Reset protected of player
        //Reset eleminate of player
        $players_in_game = player_in_game::where("IdGame", $id_game)
            ->get();
        foreach ($players_in_game as $player) {
            $p_in_g_id = $player["attributes"]["Id"];
            $player_in_game = $player::find($p_in_g_id);
            $player_in_game->Eleminate = 0;
            $player_in_game->Protected = 0;
            $player_in_game->save();
        }
    }

}
	
	
	
	
	
	