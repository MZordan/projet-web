<?php
/**
 * Created by PhpStorm.
 * User: Choffe
 * Date: 27/11/2017
 * Time: 11:16
 */
namespace app\model;
use \Illuminate\Database\Eloquent\Model as Model;
/*
 CREATE TABLE PlayerInGame (
    Id INT(10) NOT NULL AUTO_INCREMENT,
    IdPlayer INT(10)  NOT NULL,
    IdGame INT(10)  NOT NULL,
	Score INT(2),
	Eleminate INT(1),
	Protected INT(1),
    Place   int(1),
	PRIMARY KEY (Id)
)

ALTER TABLE playeringame
ADD FOREIGN KEY (IdPlayer) REFERENCES player(IdPlayer);
ALTER TABLE playeringame
ADD FOREIGN KEY (IdGame) REFERENCES game(IdGame);
 */
class PlayerInGame extends Model {

    protected $table = "PlayerInGame";
    protected $primaryKey = "Id";
    public $timestamps = false;
}