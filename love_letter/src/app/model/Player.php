<?php
/**
 * Created by PhpStorm.
 * User: Choffe
 * Date: 27/11/2017
 * Time: 11:15
 */
namespace app\model;
use \Illuminate\Database\Eloquent\Model as Model;
/*
 CREATE TABLE Player (
    IdPlayer INT(10)  NOT NULL AUTO_INCREMENT,
	Pseudo VARCHAR(15) NOT NULL,
    Pswd    Varchar(15) not null,
    PRIMARY KEY (IdPlayer)
)
 */
class Player extends Model {

    protected $table = "Player";
    protected $primaryKey = "idP";
    public $timestamps = false;
}