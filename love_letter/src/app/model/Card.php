<?php
/**
 * Created by PhpStorm.
 * User: Choffe
 * Date: 27/11/2017
 * Time: 10:49
 */
namespace app\model;
use \Illuminate\Database\Eloquent\Model as Model;
/*
 CREATE TABLE Card (
	idC INT(10) NOT NULL AUTO_INCREMENT,
    IdPlayer INT(10),
	IdGame INT(10) NOT NULL,
	State INT(1) NOT NULL,
	Type INT(2) NOT NULL,
    PRIMARY KEY (idC)
)
ALTER TABLE Card
ADD FOREIGN KEY (IdPlayer) REFERENCES player(IdPlayer);
ALTER TABLE Card
ADD FOREIGN KEY (IdGame) REFERENCES game(IdGame);
 */
class card extends Model {

    protected $table = "Card";
    protected $primaryKey = "idC";
    public $timestamps = false;
}