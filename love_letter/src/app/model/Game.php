<?php
/**
 * Created by PhpStorm.
 * User: Choffe
 * Date: 27/11/2017
 * Time: 11:07
 */
namespace app\model;
use \Illuminate\Database\Eloquent\Model as Model;
/*
 CREATE TABLE GameController (
    IdGame INT(10)  NOT NULL AUTO_INCREMENT,
    NbPlayer Int(10) not null,
	PRIMARY KEY (IdGame)
)
 */
class Game extends Model {

    protected $table = "GameController";
    protected $primaryKey = "idG";
    public $timestamps = false;
}