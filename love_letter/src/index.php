<?php
require_once '..\vendor\autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use app\model\Card;
use app\model\Game;
use app\model\PlayerInGame;
use app\model\Player;
use app\controller\GameController;

//initialisation

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$db = new DB();
$db->addConnection(parse_ini_file('conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

$_SESSION['id_joueur'] = 1;
$_SESSION['id_game'] = 69;
$_SESSION['nbr_joueur'] = 4;
$_SESSION['nbr_enJeu'] = $_SESSION['nbr_joueur'];

//ROUTER
$app->get('/', function (){
    $cartes = new GameController();
    $cartes->reset_round(69);
    // $cartes->draw(2, 69);
    //$cartes->play_card(392, 69);
    //$cartes->eleminate_player(1, 69);
})->name("root");

$app->post('/maj', function (){
	$cartes = new GameController();
	$joueurs = PlayerInGame::where('idGame', $_SESSION['id_game'])->get();
	$libres = card::where('State','0')->count();
	$retour = $_SESSION['nbr_joueur']."|".$_SESSION['position'].'|'. $libres .'|Defausse|';
	$cartesDefaussees = card::where('idGame',$_SESSION['id_game'])->where('State','8')->get();
	foreach($cartesDefaussees as $carte){
		$typeImg = $carte['Type'];
		$retour .= "../../../img/" . $typeImg . ".jpg|";
	}
	$retour .= "FinDefausse|";
	
	foreach ($joueurs as $joueur){
		$cartesEnMain = card::where('idPlayer',$joueur['IdPlayer'])->where('idGame',$_SESSION['id_game'])->where('State','1')->get();
		$cartesEnJeu = card::where('idPlayer',$joueur['IdPlayer'])->where('idGame',$_SESSION['id_game'])->where('State','2')->get();
		if($joueur['IdPlayer'] == $_SESSION['id_joueur']){
			$retour .= "Main|";
			foreach($cartesEnMain as $carte){
				$typeImg = $carte['Type'];
				$retour .= "../../../img/" . $typeImg . ".jpg|";
				$cibles = PlayerInGame::where('idGame', $_SESSION['id_game'])->where('Protected',0)->where('Eleminate',0)->get();
				foreach($cibles as $cible){
					$retour .= $cible['Position'].'|';
				}
				$retour .= 'FinCible|';
			}
			$retour .= "FinMain|";
			foreach($cartesEnJeu as $carte){
				$typeImg = $carte['Type'];
				$retour .= "../../../img/" . $typeImg . ".jpg|";
			}
			$retour .= "FinJoueur|";
		}else{
			$position = PlayerInGame::select('Position')->where('idPlayer', $joueur['IdPlayer'])->where('idGame',$_SESSION['id_game'])->first();
			$retour .= "|AutreMain|" . $position['Position'] . "|";
			foreach($cartesEnMain as $carte){
				$retour .= "dos_carte|";
			}
			$retour .= "AutreFinMain|";
			foreach($cartesEnJeu as $carte){
				$typeImg = $carte['Type'];
				$retour .= "../../../img/" . $typeImg . ".jpg|";
			}
			$retour .= "AutreFinJoueur|";
		}
	}
	$retour .= "Fin";
	
	echo($retour);
})->name("maj");

$app->post('/jouerCarte/:origine/:position', function ($origine, $position){
	$cartes = new GameController();
	$args['cible'] = $position;
	$args['card'] = $origine;
	$cartes->play_card($args);
})->name("jouerCarte");

$app->post('/login', function (){
	$cartes = new GameController();
	if (isset($_POST["user"]) && isset($_POST["pass"])) {
		$id = Player::where("pseudo", $_POST["user"])->where("pswd", $_POST["pass"])->first();
		if($id != NULL){
			$_SESSION['id_joueur'] = $id["IdPlayer"];
			?>
			<script type="text/javascript">location.href = '../app/vue/choixTable.php';</script> 
			<?php
		}else{
			?>
			<script type="text/javascript">location.href = '../app/vue/login.php';</script> 
			<?php
		}
	}
})->name("login");

$app->post('/inscri', function (){
	$cartes = new GameController();
	if (isset($_POST["user"]) && isset($_POST["pass"])) {
		if($_POST["pass"] == $_POST["pass2"]){
			$joueur = new Player;
			$joueur->pseudo = $_POST['user'];
			$joueur->pswd = $_POST['pass'];
			$joueur->save();
			$id = Player::where("pseudo", $_POST["user"])->where("pswd", $_POST["pass"])->first();
			$_SESSION['id_joueur'] = $id["IdPlayer"];
			?>
			<script type="text/javascript">location.href = '../app/vue/choixTable.php';</script> 
			<?php
		}else{
			?>
			<script type="text/javascript">location.href = '../app/vue/login.php';</script> 
			<?php
		}
		
	}else{
		?>
		<script type="text/javascript">location.href = '../app/vue/login.php';</script> 
		<?php
	}
})->name("inscri");

$app->post('/tableau', function (){
	$cartes = new GameController();
	$parties = PlayerInGame::groupBy('IdGame')->get();
	$tmp = '';
	foreach($parties as $partie){
		$joueurs = PlayerInGame::where('IdGame',$partie['IdGame'])->get();
		$tmp .='
		<tr>
		<td>';
		foreach($joueurs as $joueur){
			$pseudo = Player::where('IdPlayer',$joueur['IdPlayer'])->first();
			$tmp .= $pseudo['Pseudo'] . '| ';
		}
		$tmp .='</td>
		<td>' . $partie['IdGame'] .'</td>
		<td><button type="button" class="btn btn-info" onclick="location.href = \'../../index.php/rejoindre/' .$partie['IdGame'].' \'">Rejoindre</button></td>
	  </tr>
		';
	}
	echo($tmp);
})->name("tableau");

$app->post('/rejoindre/:id', function ($id){
		if(isset( $_SESSION['id_joueur'])){
		$cartes = new GameController();
		$_SESSION['id_game'] = $id;
		$pos = PlayerInGame::where('IdGame',$id)->first();
		$pos->max('Position');
		$_SESSION['position'] = $pos['Position']+1;
		$new = new PlayerInGame;
		$new->IdPlayer = $_SESSION['id_joueur'];
		$new->IdGame = $_SESSION['id_game'];
		$new->save();
		?>
		<script type="text/javascript">location.href = '../app/vue/table.php';</script> 
		<?php
		
	}
})->name("rejoindre");

$app->post('/draw', function (){
	$cartes = new GameController();
	$cartes->draw($_SESSION['id_joueur'],$_SESSION['id_game']);
})->name("draw");

//run
$app->run();
