
CREATE TABLE Player (
   IdPlayer INT(10)  NOT NULL AUTO_INCREMENT,
   Pseudo VARCHAR(15) NOT NULL,
   Pswd    Varchar(15) not null,
   PRIMARY KEY (IdPlayer)
);

CREATE TABLE GameController (
   IdGame INT(10)  NOT NULL AUTO_INCREMENT,
   NbPlayer Int(10) not null,
   PRIMARY KEY (IdGame)
);

 CREATE TABLE Card (
	idC INT(10) NOT NULL AUTO_INCREMENT,
    IdPlayer INT(10),
	IdGame INT(10) NOT NULL,
	State INT(1) NOT NULL,
	Type INT(2) NOT NULL,
    PRIMARY KEY (idC)
);

ALTER TABLE Card
ADD FOREIGN KEY (IdPlayer) REFERENCES player(IdPlayer);
ALTER TABLE Card
ADD FOREIGN KEY (IdGame) REFERENCES game(IdGame);


CREATE TABLE PlayerInGame (
    Id INT(10) NOT NULL AUTO_INCREMENT,
    IdPlayer INT(10)  NOT NULL,
    IdGame INT(10)  NOT NULL,
	Score INT(2),
	Eleminate INT(1),
	Protected INT(1),
    Position INT(1),
	TourDeJeu INT(1),
	PRIMARY KEY (Id)
);

ALTER TABLE playeringame
ADD FOREIGN KEY (IdPlayer) REFERENCES player(IdPlayer);
ALTER TABLE playeringame
ADD FOREIGN KEY (IdGame) REFERENCES game(IdGame);